﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> productos = new List<Producto>();
        private implements.DaoImplementsProducto daoproducto;

        public ProductoModel()
        {
            daoproducto = new implements.DaoImplementsProducto();
        }

        public List<Producto> GetProductos()
        {
            return daoproducto.findAll();
        }

        public void Populate()
        {
            productos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));
            foreach(Producto p in productos)
            {
                daoproducto.save(p);
            }
        }
        
        public void save(DataRow producto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["SKU"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripcion"].ToString();
            p.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(producto["Precio"].ToString());
            daoproducto.save(p);
        }
    }
}
