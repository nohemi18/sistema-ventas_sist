﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsDetalle : IDaoDetalle
    {
        //header
        private BinaryReader brhdetallef;
        private BinaryWriter bwhdetallef;
        //data
        private BinaryReader brddetallef;
        private BinaryWriter bwddetallef;

        private FileStream fshdetallef;
        private FileStream fsdetallef;

        private const string FILENAME_HEADER = "hdetallefac.dat";
        private const string FILENAME_DATA = "ddetallefact.dat";
        private const int SIZE = 200;

        public DaoImplementsDetalle() { }

        public void open()
        {
            try
            {
                fsdetallef = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshdetallef = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhdetallef = new BinaryReader(fshdetallef);
                    bwhdetallef = new BinaryWriter(fshdetallef);

                    brddetallef = new BinaryReader(fsdetallef);
                    bwhdetallef = new BinaryWriter(fsdetallef);

                    bwhdetallef.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhdetallef.Write(0);
                    bwhdetallef.Write(0);//k
                }
                else
                {
                    fshdetallef = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhdetallef = new BinaryReader(fshdetallef);
                    bwhdetallef = new BinaryWriter(fshdetallef);
                    brddetallef = new BinaryReader(fsdetallef);
                    bwddetallef = new BinaryWriter(fsdetallef);
                }
            }
            catch (IOException e) { throw new IOException(e.Message); }
        }
        private void close()
        {
            try
            {
                if (brddetallef != null)
                {
                    brddetallef.Close();
                }
                if (brhdetallef != null)
                {
                    brhdetallef.Close();
                }
                if (bwddetallef != null)
                {
                    bwddetallef.Close();
                }
                if (bwhdetallef != null)
                {
                    bwhdetallef.Close();
                }
                if (fsdetallef != null)
                {
                    fsdetallef.Close();
                }
                if (fshdetallef != null)
                {
                    fshdetallef.Close();
                }
            }
            catch (IOException e) { throw new IOException(e.Message);}
        }
        public bool delete(DetalleFactura t)
        {
            throw new NotImplementedException();
        }

        public List<DetalleFactura> findAll()
        {
            open();
            List<DetalleFactura> detallefact = new List<DetalleFactura>();

            brhdetallef.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhdetallef.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhdetallef.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhdetallef.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brddetallef.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brddetallef.ReadInt32();
                string cedula = brdcliente.ReadString();
                string nombres = brdcliente.ReadString();
                string apellidos = brdcliente.ReadString();
                string telefono = brdcliente.ReadString();
                string correo = brdcliente.ReadString();
                string direccion = brdcliente.ReadString();
                DetalleFactura df = new DetalleFactura();
                detallefact.Add(df);
            }

            close();
            return detallefact;
        }

        public DetalleFactura findByFactura(Factura factura)
        {
            throw new NotImplementedException();
        }

        public DetalleFactura findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<DetalleFactura> findByProducto(Producto producto)
        {
            throw new NotImplementedException();
        }

        public void save(DetalleFactura t)
        {
            throw new NotImplementedException();
        }

        public int update(DetalleFactura t)
        {
            throw new NotImplementedException();
        }
    }
}
