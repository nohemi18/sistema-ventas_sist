﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];

             ////new ProductoModel().Populate();
            foreach (Producto p in new ProductoModel().GetProductos())
            {
                DataRow drproducto = dsProductos.Tables["Producto"].NewRow();
                drproducto["Id"] = p.Id;
                drproducto["SKU"] = p.Sku;
                drproducto["Nombre"] = p.Nombre;
                drproducto["Descripcion"] = p.Descripcion;
                drproducto["Cantidad"] = p.Cantidad;
                drproducto["Precio"] = p.Precio;
                dsProductos.Tables["Producto"].Rows.Add(drproducto);
                drproducto.AcceptChanges();
            }

           EmpleadoModel.Populate();
            foreach (Empleado emp in EmpleadoModel.GetListEmpleado())
            {
                dsProductos.Tables["Empleado"].Rows.Add(emp.Id, emp.Inss, emp.Cedula, emp.Nombre, emp.Apellidos, emp.Direccion, emp.Tconvencional, emp.Tcelular, emp.Salario, emp.Sexo, emp.Nombre + " " + emp.Apellidos);
            }

            //new ClienteModel().Populate();
            foreach (Cliente client in new ClienteModel().GetListCliente())
            {
                DataRow drCliente = dsProductos.Tables["Cliente"].NewRow();
                drCliente["Id"] = client.Id;
                drCliente["Cedula"] = client.Cedula;
                drCliente["Nombres"] = client.Nombres;
                drCliente["Apellidos"] = client.Apellidos;
                drCliente["Telefono"] = client.Telefono;
                drCliente["Correo"] = client.Correo;
                drCliente["Direccion"] = client.Direccion;
                
                dsProductos.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
            }

        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsFacturas = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
