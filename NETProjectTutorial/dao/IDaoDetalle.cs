﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoDetalle : IDao<DetalleFactura>
    {
        DetalleFactura findById(int id);
        DetalleFactura findByFactura(Factura factura);
        List<DetalleFactura> findByProducto(Producto producto);
    }
}
