﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.Properties;
using NETProjectTutorial.entities;

namespace NETProjectTutorial
{
    public partial class FrmProducto : Form
    {
        private DataTable tblProductos ;
        private DataSet dsProductos;
        private BindingSource bsProductos;
        private DataRow drProducto;

        public FrmProducto()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }

       public FrmProducto(DataTable tblProductos, DataSet dsProductos)
        {
            this.TblProductos = tblProductos;
            this.DsProductos = dsProductos;
        }
        public DataRow DrProducto
        {
            set {
                drProducto = value;
                txtSku.Text =  drProducto["SKU"].ToString();
                txtName.Text = drProducto["Nombre"].ToString();
                txtDesc.Text = drProducto["Descripcion"].ToString();
                nmQuant.Value = Int32.Parse(drProducto["Cantidad"].ToString());
                msktxtPri.Text = drProducto["Precio"].ToString();
            }
            
        }

        public DataTable TblProductos
        {
            get
            {
                return tblProductos;
            }

            set
            {
                tblProductos = value;
            }
        }

        public DataSet DsProductos
        {
            get
            {
                return dsProductos;
            }

            set
            {
                dsProductos = value;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string sku, nombres, descripcion;
            int cantidad;
            double precio;

            sku = txtSku.Text;
            nombres = txtName.Text;
            descripcion = txtDesc.Text;
            cantidad = (int)nmQuant.Value;
            bool result = Double.TryParse(msktxtPri.Text, out precio);
            
            if(drProducto != null)
            {
                DataRow drNew = TblProductos.NewRow();

                int index = TblProductos.Rows.IndexOf(drProducto);
                drNew["Id"] = drProducto["Id"];
                drNew["SKU"] = sku;
                drNew["Nombre"] = nombres;
                drNew["Descripcion"] = descripcion;
                drNew["Cantidad"] = cantidad;
                drNew["Precio"] = precio;


                TblProductos.Rows.RemoveAt(index);
                TblProductos.Rows.InsertAt(drNew, index);               

            }
            else
            {
                TblProductos.Rows.Add(TblProductos.Rows.Count + 1, sku, nombres, descripcion, cantidad, precio);
            }

            Dispose();

        }

        private void FrmProducto_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = DsProductos;
            bsProductos.DataMember = DsProductos.Tables["Producto"].TableName;       
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
